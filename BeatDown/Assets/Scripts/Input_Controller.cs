﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Input_Controller : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float jumpForce;
    Vector3 lastPos;
    Rigidbody2D rb;

    public bool isGrounded;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        isGrounded = false;
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 position = new Vector3(horizontal * Time.deltaTime * speed, 0f, 0f);
        transform.position += position;


        if(vertical >= .01f && isGrounded)
        {
            rb.AddForce(new Vector2(0f, jumpForce));
            Debug.Log("Jumping");
        }

        if(vertical <= -0.01f && isGrounded)
        {
            //crouch
        }

        if(vertical <= -0.01f && !isGrounded)
        {
            //ground slam
        }
        
    }

    //TODO: Jump doens't work

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.name);

        if(other.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log(other.name);

        if (other.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }
}
